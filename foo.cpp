#include "foo.h"

template <class T>
Bar<T> Foo<T>::bar{sizeof(T) + 10};

template <class T>
int Foo<T>::call() {
    return bar.call();
}

template class Foo<int>;
template class Foo<long long>;
