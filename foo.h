#include "bar.cpp"

template <class T>
struct Foo {
    int call();
    static Bar<T> bar;    
};
