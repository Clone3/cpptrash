#include <iostream>

template <class T>
struct Bar {
    Bar(int x) {
        std::cout << "Bar(" << x << ")\n";
        keep = x;
    }

    int call() {
        return keep;
    }

    int keep;
};
