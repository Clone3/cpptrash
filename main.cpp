#include "foo.h"
#include <iostream>

int main() {
    Foo<int> a;
    Foo<long long> b;
    std::cout << a.call() << " " << b.call() << std::endl;
    
    return 0;
}
